(**************************************************************************)
(* This file is part of Store.                                            *)
(*                                                                        *)
(* Copyright (C) 2023-2024 OCamlPro SAS                                   *)
(* Copyright (C) 2023-2024 Inria                                          *)
(*                                                                        *)
(* Store is distributed under the terms of the MIT license. See the       *)
(* included LICENSE file for details.                                     *)
(**************************************************************************)

module type State = sig
  type 'a t

  val create : 'a -> 'a t
  val get : 'a t -> 'a
  val put : 'a t -> 'a -> unit
end

module type Persistent_state = sig
  type 'a t

  type 'a snapshot

  val capture : 'a t -> 'a snapshot
  val restore : 'a t -> 'a snapshot -> unit
end

module type Transactional_state = sig
  type 'a t

  type 'a transaction

  val transaction : 'a t -> 'a transaction
  val rollback : 'a t -> 'a transaction -> unit
  val commit : 'a t -> 'a transaction -> unit
end

(* Reference implementation of the [State] using references.

   Does not support the transactional interface. *)
module Ref_state : sig
  include State

  include Persistent_state with type 'a t := 'a t
end = struct
  type 'a t = 'a ref

  let create v = ref v

  let get r = !r

  let put r v = r := v

  type 'a snapshot = 'a

  let capture r = !r

  let restore r v = r := v
end

module Store_state : sig
  (** Reference implementation for the snapshot and transaction mechanisms over
      an arbitrary immutable data structure (the type parameter ['a]).

      The store (type ['a t]) is simply a pointer to a snapshot, which augment
      the state of a data structure with transactional information: each
      snapshot is a pair of the state of the data structure (which might be
      modelled for instance by a functional map) and of a list of current
      transaction (from most recent to least recent).

      Transactions are ephemeral objects: they are a pair of a snapshot and a
      (mutable) boolean indicating whether the transaction has already been
      terminated.

      A snapshot is valid and can be restored iff none of its transactions have
      been terminated yet.

      A transaction is valid if neither it nor any of its ancestors have been
      terminated yet, *and* if the current store is inside the transaction.

      This allows restoring a state outside a transaction without invalidating
      the transaction, as long as we restored back to a state inside the
      transaction prior to calling [rollback] or [commit], such as in the
      following scenario (which is valid):

        ```ocaml
        let snap_outer = capture s in
        let tran = transaction s in
        let snap_inner = capture s in
        restore s snap_outer;
        write s ...;
        let snap_outer' = capture s in
        restore s snap_inner;
        commit s tran
        ```

      Without the line `restore s snap_inner`, the `commit` would be invalid;
      but since `snap_inner` was inside the transaction, it is possible to call
      `commit` -- even though we did some work outside of the transaction
      prior to its termination. *)

  include State

  include Persistent_state with type 'a t := 'a t

  include Transactional_state with type 'a t := 'a t
end = struct
  type 'a snapshot =
    { state : 'a
    ; transactions : 'a transaction list
    }

  and 'a transaction =
    { snapshot : 'a snapshot
    ; mutable terminated : bool
    }

  type 'a t = 'a snapshot ref

  let create state = ref { state ; transactions = [] }

  let get s = !s.state

  let put s v = s := { !s with state = v }

  let capture s = !s

  let is_terminated { terminated; _ } = terminated

  (* A snapshot can be restored as long as it is not inside a terminated
     transaction. *)
  let can_restore { transactions; _ } =
    not @@ List.exists is_terminated transactions

  let restore s snap =
    if not (can_restore snap) then
      invalid_arg "restore";
    s := snap

  let transaction s =
    let transaction = { snapshot = !s; terminated = false } in
    s := { !s with transactions = transaction :: !s.transactions };
    transaction

  (* A snapshot is inside a transaction if the transaction appears in the
     snapshot's stack. *)
  let is_inside snap transaction =
    List.memq transaction snap.transactions

  (* A transaction can be terminated if it is not terminated, none of its
     ancestors are terminated (checked by [can_restore]) and the current
     store is inside the transaction. *)
  let can_terminate s ({ snapshot; terminated } as transaction) =
    not terminated && can_restore snapshot && is_inside !s transaction

  let rollback s transaction =
    if not (can_terminate s transaction) then
      invalid_arg "rollback";
    transaction.terminated <- true;
    s := transaction.snapshot

  let commit s transaction =
    if not (can_terminate s transaction) then
      invalid_arg "commit";
    transaction.terminated <- true;
    s := { !s with transactions = transaction.snapshot.transactions }
end

(* Implementation of an immutable (homogeneous) store as a map from integer keys
   to values.

   References are represented as pairs [(k, v)] where [k] is an index into the
   map and [v] is the default value for stores that do not contain [k]. *)
module Immutable_store = struct
  module IM = Map.Make (Int)

  let empty = IM.empty

  module Ref = struct
    let fresh =
      let cnt = ref 0 in
      fun () ->
        incr cnt;
        !cnt

    let make v s =
      let r = fresh () in
      (r, v), IM.add r v s

    let get (r, v) s = try IM.find r s with Not_found -> v

    let set (r, _) v s = IM.add r v s
  end
end

module Stateful_store(S : State) = struct
  let create () = S.create Immutable_store.empty

  module Ref = struct
    let make s v =
      let r, s' = Immutable_store.Ref.make v (S.get s) in
      S.put s s';
      r

    let get s r = Immutable_store.Ref.get r (S.get s)

    let set s r v = S.put s (Immutable_store.Ref.set r v (S.get s))
  end
end

(* Reference implementation *)
module R = struct
  include Store_state

  include Stateful_store(Store_state)
end

(* Candidate implementation *)
module C = Store

open Monolith

let element = sequential ()
let store = declare_abstract_type ()
let snapshot = declare_abstract_type ()
let transaction = declare_abstract_type ()
let rref = declare_abstract_type ()

let prologue () =
  let r_current = R.create () in
  let c_current = C.create () in

  declare "current" store r_current c_current;

  declare "r0" rref (R.Ref.make r_current 0) (C.Ref.make c_current 0);

  declare "r1" rref (R.Ref.make r_current 1) (C.Ref.make c_current 1)

let main fuel =
  declare "capture" (store ^> snapshot) R.capture C.capture;

  declare "restore" (store ^> snapshot ^!> unit) R.restore C.restore;
  declare "transaction" (store ^> transaction) R.transaction C.transaction;

  declare "rollback" (store ^> transaction ^!> unit) R.rollback C.rollback;

  declare "commit" (store ^> transaction ^!> unit) R.commit C.commit;

  declare "get" (store ^> rref ^> element) R.Ref.get C.Ref.get;

  declare "set" (store ^> rref ^> element ^> unit) R.Ref.set C.Ref.set;

  main ~prologue fuel

let () =
  (* The real and reference implementation raise [Invalid_argument]
     exceptions on invalid operations; their payload is unspecified
     and may differ. *)
  Monolith.override_exn_eq (fun eq exn1 exn2 ->
     match exn1, exn2 with
     | Invalid_argument _, Invalid_argument _ -> true
     | _ -> eq exn1 exn2
   )

let () =
  let fuel = 60 in
  main fuel
