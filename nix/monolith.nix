{ lib
, ocamlPackages
, fetchFromGitLab
, seq
, pprint
, afl-persistent
}:

ocamlPackages.buildDunePackage rec {
  strictDeps = true;
  pname = "monolith";
  version = "20230604";

  minimalOCamlVersion = "4.08";
  duneVersion = "3";

  src = fetchFromGitLab {
    domain = "gitlab.inria.fr";
    owner  = "fpottier";
    repo   = "monolith";
    rev    = "${version}";
    hash   = "sha256-EKXDt/IvChf0I26FLF4693AYa8jLO0ObgkOsf49p0Ow=";
  };

  propagatedBuildInputs = [ afl-persistent pprint seq ];
}
