{ lib, ocamlPackages, ... }:

ocamlPackages.buildDunePackage {
    pname = "store";
    version = "dev";
    src = lib.cleanSource ./..;

    minimalOCamlVersion = "4.05";
    duneVersion = "3";
}
