(**************************************************************************)
(* This file is part of Store.                                            *)
(*                                                                        *)
(* Copyright (C) 2023-2024 OCamlPro SAS                                   *)
(* Copyright (C) 2023-2024 Inria                                          *)
(*                                                                        *)
(* Store is distributed under the terms of the MIT license. See the       *)
(* included LICENSE file for details.                                     *)
(**************************************************************************)

let () =
  let s = Store.create () in

  let ref v = Store.Ref.make s v in
  let ( := ) r v = Store.Ref.set s r v in
  let ( ! ) r = Store.Ref.get s r in

  let no_failure f =
    fun () ->
      try f ()
      with exn ->
        prerr_endline (Printexc.to_string exn);
        assert false
  in

  (* our global state for these tests *)
  let ri = ref 0 in
  let rb = ref true in

  let check msg (i, b) =
    if !ri = i && !rb = b then ()
    else
      Printf.ksprintf failwith
        "[%d/%d, %b/%b] %s" !ri i !rb b msg
  in

  let run_and_commit ~pre ~post f =
    check "pre (old)" pre;
    Store.tentatively s (fun () ->
      check "pre (new)" pre;
      no_failure f ();
      check "post (new)" post;
    );
    check "post (old, after commit)" post;
    ()
  in
  let run_and_abort ~pre ~post f =
    try
      check "pre (old)" pre;
      Store.tentatively s (fun () ->
        check "pre (new)" pre;
        no_failure f ();
        check "post (new)" post;
        raise Exit
      )
    with Exit ->
      (* in case of abort, the postcondition
         after abort is in fact the precondition *)
      check "post (old, after abort)" pre;
      ()
  in

  check "init" (0, true);

  (* write then read *)
  ri := 1;
  rb := false;
  check "first read" (1, false);

  run_and_abort ~pre:(1, false) ~post:(3, true) begin fun () ->
    ri := 2;
    rb := true;
    ri := 3;
    assert (!ri = 3);
  end;

  run_and_commit ~pre:(1, false) ~post:(0, true) begin fun () ->
    ri := 2;
    rb := true;
    ri := 0;
  end;

  (* nested transactions *)
  run_and_abort ~pre:(0, true) ~post:(4, false) begin fun () ->

    (* version 1 *)
    ri := 2;
    rb := true;
    ri := 3;

    check "intermediate state" (3, true);

    (* first version 2, aborted *)
    run_and_abort ~pre:(3, true) ~post:(4, false) begin fun () ->
      ri := 4;
      rb := false;
    end;

    (* another version 2 which succeeds *)
    run_and_commit ~pre:(3, true) ~post:(4, false) begin fun () ->
      ri := 4;
      rb := false;
    end;

    ()
  end;

  ()
