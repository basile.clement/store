module type Store = sig
  type 'a t
  and 'a store = 'a t
  (* In these benchmarks, we assume a *homogeneous* store, which lets
     us measure the performance of both homogeneous and heterogeneous
     implementations of stores. For Vector for example, Vector is the
     natural (homogeneous) implementation, and VectorDyn introduces
     some GADT Dyn overhead to be heterogeneous. *)

  val create : unit -> 'a store

  module Ref : sig
    type 'a t
    val make : 'a store -> 'a -> 'a t
    val get : 'a store -> 'a t -> 'a
    val set : 'a store -> 'a t -> 'a -> unit
  end
end

module type Store_transactional = sig
  include Store
  val tentatively : 'a t -> (unit -> 'b) -> 'b
end

(* "Transactional" stores do not support
   nesting transactions, while "backtracking"
   stores support it. The types are the same. *)
module type Store_backtracking = Store_transactional
