#!/bin/bash
set -e

if [[ $(basename $(pwd)) != "bench" ]]
then
    echo "Error: this script must run from the ./bench subdirectory"
    exit 2
fi

OUT="$1"

if [[ -z "$OUT" ]]
then
	echo "usage: benchmark.sh results_path"
	exit 2
fi

if [[ -e "$OUT" ]]
then
	echo "Output directory '$OUT' already exists."
	exit 2
fi

mkdir -p "$OUT"

(cd .. ; dune clean)
BUILD="opam exec --switch=5.1.1+flambda -- dune build --profile=release ./real_bench.exe"

BIN=../_build/default/bench/real_bench.exe

ALIGNMENTS=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15

function param {
  local VAR=$1
  local VALS=$2
  if [[ $VALS =~ , ]]
  then
      echo "{$VAR}"
  else
      echo $VALS
  fi
}

function paramflag {
  local VAR=$1
  local VALS=$2
  if [[ $VALS =~ , ]]
  then
      echo "-L $VAR $VALS"
  fi
}

function run {
  local NAME=$1
  local BENCH=$2
  local COEFF=$3
  local PARAMS=$4
  local IMPLS=$5
  ROUNDS=$(( $COEFF * 200 ))
  echo "## $NAME"
  echo "bench: $BENCH"
  echo "impl: $IMPLS"
  echo "params: $PARAMS ROUNDS=$ROUNDS"
  echo
  for COMP in flambda
  do
      local FILE=$OUT/bench.$NAME
      local CMD="IMPL=$(param impl $IMPLS) BENCH=$(param bench $BENCH) ROUNDS=$ROUNDS $PARAMS $BIN"
      if [[ $IMPLS =~ , ]]; then
        local CMDNAME={impl}
      else
        local CMDNAME={bench}
      fi
      hyperfine \
	  -s "ALIGNMENT_FUZZ=$(param fuzz $ALIGNMENTS) $BUILD" \
          --sort mean-time \
          --warmup 10 \
          $(paramflag impl $IMPLS) \
          $(paramflag bench $BENCH) \
          $(paramflag fuzz $ALIGNMENTS) \
          --export-markdown $FILE.md \
          --export-json $FILE.json \
          --command-name "$CMDNAME" \
          "$CMD"
      echo
      echo >> $FILE.md
      echo "<!--" >> $FILE.md
      echo $CMD >> $FILE.md
      echo "-->" >> $FILE.md
  done
}

PARAMS="NCREATE=10 NWRITE=12 NREAD=16"
GETPARAMS="NCREATE=10 NWRITE=0 NREAD=18"
SETFEWPARAMS="NCREATE=10 NWRITE=6 NREAD=0"
SET1PARAMS="NCREATE=10 NWRITE=10 NREAD=0"
SET16PARAMS="NCREATE=10 NWRITE=14 NREAD=0"

CAPTUREHEAVYPARAMS="NCREATE=2 NWRITE=4 NREAD=6"
CAPTUREHEAVYPARAMSLARGESUPPORT="NCREATE=10 NWRITE=4 NREAD=6"

echo "# Per-benchmark results"

echo "## Raw"
echo "No transactions at all."

run Raw Raw 4 "$PARAMS" \
  Ref,Store,TransactionalRef,BacktrackingRef,Facile,Colibri2,Vector,Map

run Raw.Get Raw 2 "$GETPARAMS" \
  Ref,Store,TransactionalRef,BacktrackingRef,Facile,Colibri2,Vector,Map

run Raw.Set Raw 100 "$SET1PARAMS" \
  Ref,Store,TransactionalRef,BacktrackingRef,Facile,Vector,Map

# we do not include Map to avoid taking too long
# (we can thus use a higher coefficient)

echo "### Raw (Transactional)"
echo "Same as Raw, but under just one transaction."
echo "(probably not very interesting; we remove Map to reduce benchmark time)"

run Transactional-raw Transactional-raw 5 "$PARAMS" \
  Store,TransactionalRef,BacktrackingRef,Facile,Vector

run Transactional-raw.Get Transactional-raw 1 "$GETPARAMS" \
  Store,TransactionalRef,BacktrackingRef,Facile,Vector

run Transactional-raw.Set Transactional-raw 64 "$SET1PARAMS" \
  Store,TransactionalRef,BacktrackingRef,Facile,Vector

echo "## Transactional"
echo "One transaction per round."

echo "Mixed workload."
run Transactional-commit Transactional-commit 3 "$PARAMS" \
  Store,TransactionalRef,BacktrackingRef,Facile,Colibri2,Vector,Map

run Transactional-abort Transactional-abort 3 "$PARAMS" \
  Store,TransactionalRef,BacktrackingRef,Facile,Colibri2,Vector,Map

echo "Get-only workload."
run Transactional-commit.Get Transactional-commit 1 "$GETPARAMS" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

run Transactional-abort.Get Transactional-abort 1 "$GETPARAMS" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

echo "Set few: set 1/16th of references (once) at each round."
run Transactional-commit.SetFew Transactional-commit 200 "$SETFEWPARAMS" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

run Transactional-abort.SetFew Transactional-abort 200 "$SETFEWPARAMS" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

echo "Set1: set each reference once per round."
run Transactional-commit.Set1 Transactional-commit 32 "$SET1PARAMS" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

run Transactional-abort.Set1 Transactional-abort 32 "$SET1PARAMS" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

echo "Set16: set each reference 16 times per round."
run Transactional-commit.Set16 Transactional-commit 3 "$SET16PARAMS" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

run Transactional-abort.Set16 Transactional-abort 3 "$SET16PARAMS" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

echo "## Backtracking"
echo "Nested transactions."

# for full backtracking we want to compare with Map
run Backtracking-abort Backtracking-abort 5 "$PARAMS" \
  Store,BacktrackingRef,Facile,Vector,Map

run Backtracking-commit Backtracking-commit 5 "$PARAMS" \
  Store,BacktrackingRef,Facile,Vector,Map

# BacktrackingRef is only semi-persistent, so it is not tested here
run Backtracking-persistent Backtracking-persistent 3 "$PARAMS" \
  Store,Vector,Map

echo
echo
echo "# Extra benchmarks"

echo "Try a case where there is a lot more capture/restore relatively to get/set."
echo "Small support (small number of references)"
run Transactional-commit.capture_heavy.small_support \
  Transactional-commit 700 "$CAPTUREHEAVYPARAMS" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

run Transactional-abort.capture_heavy.small_support \
  Transactional-abort 700 "$CAPTUREHEAVYPARAMS" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

echo "Try a case where there is a lot more capture/restore relatively to get/set."
echo "Large support (large number of references, only a few touched each round)"
run Transactional-commit.capture_heavy.large_support \
  Transactional-commit 700 "$CAPTUREHEAVYPARAMSLARGESUPPORT" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

run Transactional-abort.capture_heavy.large_support \
  Transactional-abort 700 "$CAPTUREHEAVYPARAMSLARGESUPPORT" \
  Store,TransactionalRef,BacktrackingRef,Colibri2,Facile,Vector,Map

echo "Compare the cost of the semi-persistent vs. persistent interfaces of Store."
echo "Standard parameters."
run API \
  Backtracking-abort,Backtracking-persistent \
  5 "$PARAMS" Store

echo "Compare the cost of the semi-persistent vs. persistent interfaces of Store."
echo "Capture-heavy parameters."
run API.capture-heavy \
  Backtracking-abort,Backtracking-persistent \
  700 "$CAPTUREHEAVYPARAMS" Store
