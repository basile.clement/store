(* a Store interface defined on top of colibri2/context.ml *)

module Context = Colibri2.Context

type 'a store = Context.context
type 'a t = 'a store

let create () =
  Context.create ()

let copy _ctx = assert false

module Ref = struct
  type 'a t = 'a Context.Ref.t

  let make ctx v =
    Context.Ref.create (Context.creator ctx) v
  let get _ctx r =
    Context.Ref.get r
  let set _ctx r v =
    Context.Ref.set r v
end

let eq _ctx r1 r2 = (r1 == r2)

let[@inline always] transaction ctx =
  let bp = Context.bp ctx in
  Context.push ctx;
  bp

let[@inline always] rollback _ctx bp =
  try Context.pop bp with Context.AlreadyPoped -> invalid_arg "rollback"

let tentatively ctx f =
  let t = transaction ctx in
  match f () with
  | v ->
     (* we do *not* pop the backtracking point as this would
        abort/rollback instead of commit the changes from [f ()]. *)
     v
  | exception e ->
     let b = Printexc.get_raw_backtrace() in
     rollback ctx t;
     Printexc.raise_with_backtrace e b

